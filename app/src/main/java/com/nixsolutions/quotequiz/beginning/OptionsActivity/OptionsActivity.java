package com.nixsolutions.quotequiz.beginning.OptionsActivity;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.nixsolutions.quotequiz.beginning.InfoDialog;
import com.nixsolutions.quotequiz.beginning.MainActivity;
import com.nixsolutions.quotequiz.beginning.MainScreen;
import com.nixsolutions.quotequiz.beginning.R;


public class OptionsActivity extends Activity {

    private FirebaseAnalytics mFirebaseAnalytics;
    private final String FBA_TYPE_OF_GAME_EVENT = "type_of_game";
    private final String FBA_TYPE_OF_GAME_MOVIES = "movies";
    private final String FBA_TYPE_OF_GAME_BOOKS = "books";
    private boolean animated = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_options);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences sharedPref = this.getSharedPreferences("score_file", MODE_PRIVATE);
        int maxscore = sharedPref.getInt("saved_max_score", -100000);
        int level = sharedPref.getInt("level", 1);

        if(maxscore == -100000)
            maxscore = 0;

        Typeface face2 = Typeface.createFromAsset(getAssets(), "fonts/euphorigenic.ttf");

        TextView bestScore_tv = (TextView) findViewById(R.id.bestScore);
        bestScore_tv.setText("Best Score " + maxscore);
        bestScore_tv.setTypeface(face2);

        TextView level_tv = (TextView) findViewById(R.id.level);
        level_tv.setText("Level " + level);
        level_tv.setTypeface(face2);
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if(!animated) {
            YoYo.with(Techniques.BounceInDown).duration(1000).playOn(findViewById(R.id.logo));
            YoYo.with(Techniques.BounceInLeft).duration(1000).playOn(findViewById(R.id.btn_books));
            YoYo.with(Techniques.BounceInRight).duration(1000).playOn(findViewById(R.id.btn_movies));
            animated = true;
        }
    }

    public void play_movies(View v) {
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, FBA_TYPE_OF_GAME_MOVIES);
        mFirebaseAnalytics.logEvent(FBA_TYPE_OF_GAME_EVENT, bundle);

        v.playSoundEffect(android.view.SoundEffectConstants.CLICK);
        Intent i = new Intent(this, MainActivity.class);
        i.putExtra(MainScreen.SOURCE_DATABASE, "MOVIES");
        startActivity(i);
    }

    public void play_books(View v) {
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, FBA_TYPE_OF_GAME_BOOKS);
        mFirebaseAnalytics.logEvent(FBA_TYPE_OF_GAME_EVENT, bundle);

        v.playSoundEffect(android.view.SoundEffectConstants.CLICK);
        Intent i = new Intent(this, MainActivity.class);
        i.putExtra(MainScreen.SOURCE_DATABASE, "BOOKS");
        startActivity(i);
    }

    public void infoLevel(View v) {
        InfoDialog d = new InfoDialog(this, getString(R.string.info_lvl));
        d.show();
    }

    public void infoScore(View v) {
        InfoDialog d = new InfoDialog(this, getString(R.string.info_score));
        d.show();
    }

}
