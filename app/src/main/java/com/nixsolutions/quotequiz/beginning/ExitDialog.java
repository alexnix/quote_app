package com.nixsolutions.quotequiz.beginning;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by rr on 10-Jul-16.
 */
public class ExitDialog extends Dialog {
    private int layout;
    private boolean animated = false;
    Activity parrent;

    @BindView(R.id.message)
    TextView message;

    @BindView(R.id.yes)
    Button yes;

    @BindView(R.id.no)
    Button no;

    public ExitDialog(Activity parrent) {
        super(parrent);
        this.parrent = parrent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_exit);
        ButterKnife.bind(this);
        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        Typeface face1 = Typeface.createFromAsset(parrent.getAssets(), "fonts/libel-suit-rg.ttf");
        yes.setTypeface(face1);
        no.setTypeface(face1);

        Typeface face2 = Typeface.createFromAsset(parrent.getAssets(), "fonts/euphorigenic.ttf");
        message.setTypeface(face2);

    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if(!animated) {
            YoYo.with(Techniques.BounceInUp).duration(800).playOn(findViewById(R.id.dialog_wrapper));
            animated = true;
        }
    }

    @OnClick(R.id.no)
    public void no(View v) {
        dismiss();
    }

    @OnClick(R.id.yes)
    public void yes(View v) {
        parrent.finish();
        dismiss();
    }
}
