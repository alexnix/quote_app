package com.nixsolutions.quotequiz.beginning;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;
import android.widget.Button;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

import static android.content.Context.MODE_PRIVATE;


/**
 * Created by m17336 on 6/13/2016.
 */
public class MainScreenPresenter {
    private static final int LEVEL_UP = 10;
    private MainScreen view;
    private ArrayList<Quote> quotes;
    public int currentQuote, score, currentBest, level_counter, level;
    private ArrayList<Integer> options;
    private QuoteHelper quoteHelper;

    public MainScreenPresenter(MainScreen view){
        this.view = view;
        this.quoteHelper = new QuoteHelper((Context)view, view.getSourceDatabaseFromIntent());
        currentBest = getMaxScore();
        level = getLevelFromSp();
        level_counter = getLevelCounterFromSp();
        quotes = new ArrayList<>();
        options = new ArrayList<>();
        options.add(1);
        options.add(2);
        options.add(3);
    }

    public void getQuote() {

        receiveQuote(quoteHelper.getRandomQuote());

    }

    public void check(Button btn) {
        if( btn.getText().toString().equals(quotes.get(currentQuote-1).getMovie()) ){
            // make button greeen
            view.correct(btn);
            // update score
            score++;
            // Update level counter
            if(!quotes.get(currentQuote-1).isAnswered()) {
                quoteHelper.markAsAnswered(quotes.get(currentQuote-1).getId());
                level_counter++;
                if(level_counter == LEVEL_UP) {
                    level++;
                    updateLevel();
                    view.showLevel(level);
                    view.showLevelUpDialog();
                } else {
                    updateLevelCounter();
                }
            }
        } else {
            // make button red
            view.incorrect(btn);
            // update score
            score--;
        }
        if(score > currentBest) {
            setMaxScore(score);
            currentBest = score;
        }
        view.showScore(score);
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                getQuote();
            }
        }, 500);
    }

    public int getMaxScore() {
        SharedPreferences sharedPref = view.getContext().getSharedPreferences("score_file", MODE_PRIVATE);
        return sharedPref.getInt("saved_max_score", -1000);
    }

    public void setMaxScore(int score) {
        SharedPreferences sharedPref = view.getContext().getSharedPreferences("score_file", MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt("saved_max_score", score);
        editor.commit();
    }

    private void updateLevel() {
        SharedPreferences sharedPref = view.getContext().getSharedPreferences("score_file", MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt("level", level);
        editor.putInt("level_counter", level_counter);
        editor.commit();
    }

    private void updateLevelCounter() {
        SharedPreferences sharedPref = view.getContext().getSharedPreferences("score_file", MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt("level_counter", level_counter);
        editor.commit();
    }

    public int getLevelFromSp() {
        SharedPreferences sharedPref = view.getContext().getSharedPreferences("score_file", MODE_PRIVATE);
        return sharedPref.getInt("level", 1);
    }

    public int getLevelCounterFromSp() {
        SharedPreferences sharedPref = view.getContext().getSharedPreferences("score_file", MODE_PRIVATE);
        return sharedPref.getInt("level_counter", 0);
    }

    private void receiveQuote(Quote quote) {
        quotes.add(quote);
        if(quotes.size() <= 3)
            getQuote();
        else
            showQuestion(quotes.get(currentQuote++));
    }

    private void showQuestion(Quote q) {
        this.view.changeText(q.getQuote());
        Collections.shuffle(options);
        this.view.setOption(options.get(0), q.getMovie());
        Quote q2 = getDifferentQuote(q);
        this.view.setOption(options.get(1), q2.getMovie());
        Quote q3 = getDifferentQuote(q, q2);
        this.view.setOption(options.get(2), q3.getMovie());

    }

    private Quote getDifferentQuote(Quote q1) {
        Quote q2;
        while( (q2 = quotes.get(new Random().nextInt(quotes.size()))).getMovie().equals(q1.getMovie()) );
        return q2;
    }

    private Quote getDifferentQuote(Quote q1, Quote q2) {
        Quote q3;
        q3 = quotes.get(new Random().nextInt(quotes.size()));
        while( q3.getMovie().equals(q1.getMovie()) || q3.getMovie().equals(q2.getMovie()) ){
            q3 = quotes.get(new Random().nextInt(quotes.size()));
        }
        return q3;
    }

}
