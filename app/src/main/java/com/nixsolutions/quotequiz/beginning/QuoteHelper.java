package com.nixsolutions.quotequiz.beginning;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

/**
 * Created by rr on 22-Apr-17.
 */

public class QuoteHelper extends SQLiteAssetHelper {

    private static final String DATABASE_NAME = "quotes.db";
    private static final int DATABASE_VERSION = 5;

    private final String database;

    public QuoteHelper(Context context, String database) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        setForcedUpgrade();
        this.database = database;
    }

    // Getting single quote
    Quote getRandomQuote() {
        String selectQuery = "SELECT * FROM " + this.database + " WHERE id IN (SELECT id FROM " + this.database + " ORDER BY RANDOM() LIMIT 1)";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        cursor.moveToFirst();
        Quote quote = new Quote(cursor.getInt(0), cursor.getString(1), cursor.getString(2), cursor.getInt(3) > 0);

        return quote;
    }

    public void markAsAnswered(int id) {
        String updateQuery = "UPDATE " + this.database + " SET answered_correct = 1 WHERE id == " + id;
        SQLiteDatabase db = this.getWritableDatabase();
        db.rawQuery(updateQuery, null);
    }
}