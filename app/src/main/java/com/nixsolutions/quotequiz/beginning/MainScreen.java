package com.nixsolutions.quotequiz.beginning;

import android.content.Context;
import android.view.View;
import android.widget.Button;

/**
 * Created by m17336 on 6/13/2016.
 */
public interface MainScreen {
    String SOURCE_DATABASE = "source_database";

    Context getContext();
    void changeText(String txt);
    void setOption(int no, String opt);
    void correct(Button btn);
    void incorrect(Button btn);
    void showScore(int score);
    void showLevel(int level);
    String getSourceDatabaseFromIntent();
    void showLevelUpDialog();

}
