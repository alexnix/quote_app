package com.nixsolutions.quotequiz.beginning;

/**
 * Created by M17336 on 6/17/2016.
 */
public class Quote {

    private int id;
    private String movie;
    private String quote;
    private boolean answered;

    public Quote(int id, String movie, String quote, boolean answered) {
        this.id = id;
        this.movie = movie;
        this.quote = quote;
        this.answered = answered;
    }

    public int getId() {
        return id;
    }

    public String getMovie() {
        return movie;
    }

    public String getQuote() {
        return quote;
    }

    public boolean isAnswered() {
        return answered;
    }
}
