package com.nixsolutions.quotequiz.beginning;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.firebase.analytics.FirebaseAnalytics;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends Activity implements MainScreen {
    MediaPlayer mp;

    @BindView(R.id.text)
    TextView text;

    @BindView(R.id.score)
    TextView score;

    @BindView(R.id.level)
    TextView level;

    @BindView(R.id.btnOption1)
    Button option1;

    @BindView(R.id.btnOption2)
    Button option2;

    @BindView(R.id.btnOption3)
    Button option3;

    private FirebaseAnalytics mFirebaseAnalytics;
    private MainScreenPresenter presenter;
    private ConnectionUnavailableDialog connectionUnavailableDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        ButterKnife.bind(this);

        Typeface face1 = Typeface.createFromAsset(getAssets(), "fonts/libel-suit-rg.ttf");
        option1.setTypeface(face1);
        option2.setTypeface(face1);
        option3.setTypeface(face1);

        Typeface face2 = Typeface.createFromAsset(getAssets(), "fonts/euphorigenic.ttf");
        text.setTypeface(face2);
        score.setTypeface(face2);
        level.setTypeface(face2);

        mp = MediaPlayer.create(this, R.raw.click);

        AdView mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice("F05DDD9BC48229C101845867C8B66A7E")
                .build();
        mAdView.loadAd(adRequest);

        presenter = new MainScreenPresenter(this);
        presenter.getQuote();

    }

    public void check(View view) {
        try {
            mp.prepare();
        } catch (Exception e) {}
        mp.start();
        presenter.check((Button) view);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
    return true;
}

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void changeText(String txt) {
        this.text.setText(txt);
    }

    @Override
    public void setOption(int no, String opt) {
        switch (no) {
            case 1:
                option1.setText(opt);
                option1.setVisibility(View.VISIBLE);
                option1.setBackground(getResources().getDrawable(R.drawable.btn1bg));
                break;
            case 2:
                option2.setText(opt);
                option2.setVisibility(View.VISIBLE);
                option2.setBackground(getResources().getDrawable(R.drawable.btn2bg));
                break;
            case 3:
                option3.setText(opt);
                option3.setVisibility(View.VISIBLE);
                option3.setBackground(getResources().getDrawable(R.drawable.btn3bg));
                break;
        }

    }

    @Override
    public void correct(Button btn) {
        switch (btn.getId()) {
            case R.id.btnOption1:
                btn.setBackground(getResources().getDrawable(R.drawable.btn1bg_green));
                break;
            case R.id.btnOption2:
                btn.setBackground(getResources().getDrawable(R.drawable.btn2bg_green));
                break;
            case R.id.btnOption3:
                btn.setBackground(getResources().getDrawable(R.drawable.btn3bg_green));
                break;
        }
    }

    @Override
    public void incorrect(Button btn) {
        switch (btn.getId()) {
            case R.id.btnOption1:
                btn.setBackground(getResources().getDrawable(R.drawable.btn1bg_red));
                break;
            case R.id.btnOption2:
                btn.setBackground(getResources().getDrawable(R.drawable.btn2bg_red));
                break;
            case R.id.btnOption3:
                btn.setBackground(getResources().getDrawable(R.drawable.btn3bg_red));
                break;
        }
    }

    @Override
    public void showScore(int score) {
        this.score.setText("Score " + score);
    }

    @Override
    public void showLevel(int level) {
        this.level.setText("Level " + level);
    }

    @Override
    public void onBackPressed() {
        ExitDialog d = new ExitDialog(this);
        d.show();
    }

    @Override
    public String getSourceDatabaseFromIntent() {
        return getIntent().getStringExtra(MainScreen.SOURCE_DATABASE);
    }

    @Override
    public void showLevelUpDialog() {
        Bundle bundle = new Bundle();
        bundle.putInt(FirebaseAnalytics.Param.LEVEL, presenter.level);
        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.LEVEL_UP, bundle);
        LevelUpDialog d = new LevelUpDialog(this);
        d.show();
    }

    @OnClick(R.id.infoLvl)
    public void showInfoLvl(View v) {
        InfoDialog d = new InfoDialog(this, getString(R.string.info_lvl));
        d.show();
    }

    @OnClick(R.id.infoScore)
    public void showInfoScore(View v) {
        InfoDialog d = new InfoDialog(this, getString(R.string.info_score));
        d.show();
    }
}
