package com.nixsolutions.quotequiz.beginning;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by rr on 17-Jul-16.
 */
public class ConnectionUnavailableDialog extends Dialog {
    private Activity parrent;
    private boolean visible;

    @BindView(R.id.title)
    TextView title;

    @BindView(R.id.message)
    TextView message;

    public ConnectionUnavailableDialog(Activity parrent){
        super(parrent);
        this.parrent = parrent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_connection_unavailable);
        ButterKnife.bind(this);
        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        Typeface face1 = Typeface.createFromAsset(parrent.getAssets(), "fonts/libel-suit-rg.ttf");
        title.setTypeface(face1);
        message.setTypeface(face1);
    }

    @Override
    public void show() {
        super.show();
        this.visible = true;
    }

    @Override
    public void hide() {
        super.hide();
        this.visible = false;
    }

    public boolean isVisible() {
        return visible;
    }
}
