package com.nixsolutions.quotequiz.beginning;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by M17336 on 12/7/2017.
 */

public class InfoDialog extends Dialog {
    private boolean animated = false;
    private String info;
    private Context ctx;

    public InfoDialog(@NonNull Context context,@NonNull String info) {
        super(context);
        this.info = info;
        ctx = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_info);
        ButterKnife.bind(this);
        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        TextView info_tv = (TextView) findViewById(R.id.info_txt);
        info_tv.setText(info);

        Typeface face = Typeface.createFromAsset(ctx.getAssets(), "fonts/euphorigenic.ttf");
        info_tv.setTypeface(face);
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if(!animated) {
            YoYo.with(Techniques.BounceInUp).duration(800).playOn(findViewById(R.id.dialog_wrapper));
            animated = true;
        }
    }

    @OnClick(R.id.okBtn)
    public void ok(View v){
        dismiss();
    }
}
